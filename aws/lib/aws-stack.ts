import * as cdk from 'aws-cdk-lib';
import * as ecs from 'aws-cdk-lib/aws-ecs';
import { ContainerImage } from 'aws-cdk-lib/aws-ecs';
import { ApplicationLoadBalancedFargateService } from 'aws-cdk-lib/aws-ecs-patterns';
import { ApplicationProtocol } from 'aws-cdk-lib/aws-elasticloadbalancingv2';
import { HostedZone } from 'aws-cdk-lib/aws-route53';
import { Construct } from 'constructs';

export class AwsStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const toremoZone = HostedZone.fromHostedZoneAttributes(this, "zone-toremo-com", {
      hostedZoneId: "Z162CFB4O3ADPJ",
      zoneName: "toremo.com"
    });

    // Task definition for the microservice
    const helloworldTask = new ecs.FargateTaskDefinition(this, "helloworld", {
      // Be careful about the parameter combination: it can only be one of those in https://docs.aws.amazon.com/AmazonECS/latest/developerguide/AWS_Fargate.html
      cpu: 256,
      memoryLimitMiB: 512
    });
    helloworldTask.addContainer("helloworld", {
      image: ContainerImage.fromRegistry("registry.gitlab.com/toremo/helloworld:latest"),
      portMappings: [{
        containerPort: 3000,
        protocol: ecs.Protocol.TCP,
      }],
    });

    // Cluster to execute the task
    const cluster = new ecs.Cluster(this, "toremo-daniel-test", {
      clusterName: "toremo-daniel-test"
    });

    // Service that deploys the task definition in the cluster
    const service = new ApplicationLoadBalancedFargateService(this, 'helloworld_service', {
      serviceName: "helloworld-service",
      cluster: cluster,
      taskDefinition: helloworldTask,
      publicLoadBalancer: true,
      desiredCount: 3,
      listenerPort: 443,
      protocol: ApplicationProtocol.HTTPS,
      domainZone: toremoZone,
      domainName: "daniel.toremo.com"
    });

    // ---------------------------------------------
    // Pipeline to automate the provisioning process
    // ---------------------------------------------
  }
}
