#!/bin/bash

# Stop at the first error and be verbose

s3_bucket=toremo-daniel-test

# Note - I would rather zip the file and accompany it with an SHA256 signature than copying the whole thing in an S3 bucket. It is very expensive and slow to sync.
aws s3 cp s3://$s3_bucket/bundle.js /tmp

diff -q /tmp/bundle.js bundle.js

if [$? -eq 1 ]; then
        if [[ -e "service.pid" ]]; then
                kill -9 $(cat service.pid)
        fi
        nohup node index.js &
fi