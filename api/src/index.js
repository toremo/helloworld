import express from "express";

const app = express();

app.get('/version', (_, res) => {
    res.send('0.0.5');
});

app.get('/', (_, res) => {
    res.send('Happy Sunday, Daniel! Take 2');
});

app.listen(3000, () => {
    console.info("Hello world service started on port 3000");
});